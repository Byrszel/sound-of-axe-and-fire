package controller;

import java.util.ArrayList;

import model.Benutzer;
import model.Musik;															// Kraftwerk - Das Model
import model.SQL_Datenbank;
import model.Vote;
import view.StartGUI;
import view.VoteGUI;

public class QuickMath {													// 2 + 2 = 4 -------->  - 1 = 3

	private ArrayList<Musik> musikListe = new ArrayList<Musik>();			// deutsche Party Hits
	private ArrayList<Benutzer> benutzerListe = new ArrayList<Benutzer>();
	private String angemeldeterNutzer;
	private String gastgeber = "Friedrich";
	
	public QuickMath() {
		super();
		this.musikListe = SQL_Datenbank.getMusikAusDatenbank();
		this.benutzerListe = SQL_Datenbank.getBenutzerAusDatenbank();
	}

	public ArrayList<Musik> getMusikListe() {
		return SQL_Datenbank.getMusikAusDatenbank();
	}

	public void addMusikListe(Musik eingabe) {
		SQL_Datenbank.addMusikZuDatenbank(eingabe);
		this.musikListe.add(eingabe);
	}
	
	public ArrayList<Benutzer> getBenutzerListe() {
		return SQL_Datenbank.getBenutzerAusDatenbank();
	}

	public boolean addBenutzerListe(Benutzer eingabe) {
		if (eingabe.getAdelsgeschlecht().equals("")) {
			eingabe.setAdelsgeschlecht(null);
		}
		if (SQL_Datenbank.addBenutzerZuDatenbank(eingabe)) {
			this.benutzerListe.add(eingabe);
			return true;
		}else return false;
	}
	
	public Benutzer searchBenutzer(String id) {
		Benutzer rueckgabe = null;
		for (int i = 0; i < benutzerListe.size(); i++) {
			if (benutzerListe.get(i).getBenutzerID().equals(id)) {
				rueckgabe = benutzerListe.get(i);
			}
		}
		return rueckgabe;
	}
	
	public boolean doesBenutzerIdExist(String eingabe) {
		boolean rueckgabe = false;
		for (int i = 0; i < benutzerListe.size(); i++) {
			if (benutzerListe.get(i).getBenutzerID().equals(eingabe)) {
				rueckgabe = true;
			}
		}
		return rueckgabe;
	}
	
	public boolean login(String benutzerID) {
		if (doesBenutzerIdExist(benutzerID)) {
			angemeldeterNutzer = benutzerID;
			StartGUI.setGUItoLogin();
			return true;
		}
		return false;
	}
	
	public void logout() {
		angemeldeterNutzer = null;
		StartGUI.setGUItoLogout();
	}
	
	public boolean istJemandAngemeldet() {
		if (angemeldeterNutzer == null) {
			return false;
		}else return true;
	}

	public String getAngemeldeterNutzer() {
		return angemeldeterNutzer;
	}
	
	public Vote[] getVotesOfCurrentUser() {
		int indexRueckgabe = -1;
		for (int i = 0; i < benutzerListe.size(); i++) {
			if (benutzerListe.get(i).getBenutzerID().equals(angemeldeterNutzer)) {
				indexRueckgabe = i;
			}
		}
		return benutzerListe.get(indexRueckgabe).getMeineVotes();
	}
	
	public void voten(int newSongID) {
		if (istJemandAngemeldet()) {
			for (int i = Benutzer.maxVotesProPerson - 1; i > 0; i--) {
				searchBenutzer(angemeldeterNutzer).getMeineVotes()[i].setSongID(searchBenutzer(angemeldeterNutzer).getMeineVotes()[i-1].getSongID());
			}
			searchBenutzer(angemeldeterNutzer).getMeineVotes()[0].setSongID(newSongID);
			VoteGUI.votenGuiUpdate();
		}
	}
	
	public boolean isGastgeber() {
		if (istJemandAngemeldet()) {
			if (getAngemeldeterNutzer().equals(gastgeber)) {
				return true;
			}else return false;
		}else return false;
	}
	
	public boolean isAdelsgeschlecht() {
		if (istJemandAngemeldet()) {
			if (searchBenutzer(angemeldeterNutzer).getAdelsgeschlecht() != null) {
				return true;
			}else return false;
		}else return false;
	}
	
	public int[] getVotes() {
		int[] votes = new int[musikListe.size()];
		for (int i = 0; i < votes.length; i++) {
			votes[i] = 0;
		}
		for (int i = 0; i < this.benutzerListe.size(); i++) {
			for (int j = 0; j < Benutzer.maxVotesProPerson; j++) {
				if (benutzerListe.get(i).getMeineVotes()[j].getSongID() != -1) {
					votes[benutzerListe.get(i).getMeineVotes()[j].getSongID()]++;
				}
			}
		}
		return votes;
	}

	
	
}