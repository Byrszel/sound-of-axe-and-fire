package model;

import view.StartGUI;

public class Benutzer {

	public final static int maxVotesProPerson = 5;
	private String benutzerID;
	private String passwort;
	private String adelsgeschlecht;
	private Vote[] meineVotes = new Vote[maxVotesProPerson];

	public Benutzer(String benutzerID, String adelsgeschlecht) {
		this.setBenutzerID(benutzerID);
		this.setAdelsgeschlecht(adelsgeschlecht);
		for (int i = 0; i < meineVotes.length; i++) {
			meineVotes[i] = new Vote();
		}
	}

	public String getBenutzerID() {
		return benutzerID;
	}

	public void setBenutzerID(String benutzerID) {
		this.benutzerID = benutzerID;
	}

	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}

	public Vote[] getMeineVotes() {
		return meineVotes;
	}

	public String getAdelsgeschlecht() {
		return adelsgeschlecht;
	}

	public void setAdelsgeschlecht(String adelsgeschlecht) {
		this.adelsgeschlecht = adelsgeschlecht;
	}
	
	
	public void voten(Musik eingabe) {
		for (int i = 1; i < meineVotes.length; i++) {
			meineVotes[i-1].setSongID(meineVotes[i].getSongID());
		}
		meineVotes[meineVotes.length].setSongID(StartGUI.controller.getMusikListe().indexOf(eingabe));
	}
}
