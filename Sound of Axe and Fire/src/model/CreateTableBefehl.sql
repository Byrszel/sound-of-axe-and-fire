
Insert into t_votes( f_BenutzerName, f_Song_ID,anzahlVote)
Values ('Max','5','2');
Insert into t_votes(f_BenutzerName, f_Song_ID,anzahlVote)
Values ('Elly','3','3');
Insert into t_votes( f_BenutzerName, f_Song_ID,anzahlVote)
Values ('Lily','4','1');
Insert into t_votes( f_BenutzerName, f_Song_ID,anzahlVote)
Values ('Lars','1','3');
Insert into t_votes( f_BenutzerName, f_Song_ID,anzahlVote)
Values ('Alina','2','4');

INSERT INTO t_benutzer(p_BenutzerName)
VALUES ('Max');
INSERT INTO t_benutzer(p_BenutzerName)
VALUES ('Lily');
INSERT INTO t_benutzer(p_BenutzerName)
VALUES ('Alina');
INSERT INTO t_benutzer(p_BenutzerName)
VALUES ('Elly');
INSERT INTO t_benutzer(p_BenutzerName)
VALUES ('Lars');

INSERT INTO t_musik(bandName,genre,titelName)
VALUES ('ApoRed','Pop','Love');
INSERT INTO t_musik(bandName,genre,titelName)
VALUES ('KSfreak','Rap','Nice');
INSERT INTO t_musik(bandName,genre,titelName)
VALUES ('Neffex','Pock','Hoch Hinaus');
INSERT INTO t_musik(bandName,genre,titelName)
VALUES ('Queen','R&B','Tanzen');
INSERT INTO t_musik(bandName,genre,titelName)
VALUES ('PewDiePie','Metal','Schlangen');

	
-----------------------------------------------------------------------------------------------------

CREATE TABLE `t_benutzer` (
	`p_BenutzerName` VARCHAR(150) NOT NULL,
	`Passwort` VARCHAR(30) NOT NULL,
	`hausName` VARCHAR(50) NOT NULL,
	`gastgeber` TINYINT(1) NULL DEFAULT NULL,
	PRIMARY KEY (`p_BenutzerName`)
)

CREATE TABLE `t_musik` (
	`p_Song_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`bandName` VARCHAR(100) NULL DEFAULT NULL,
	`genre` VARCHAR(100) NULL DEFAULT NULL,
	`titelName` VARCHAR(100) NULL DEFAULT NULL,
	PRIMARY KEY (`p_Song_ID`)
	
)
;
CREATE TABLE `t_votes` (
	`P_Zeitpunkt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`f_BenutzerName` VARCHAR(150) NOT NULL,
	`f_Song_ID` INT(11) NOT NULL,
	`anzahlVote` INT(11) NOT NULL,
	PRIMARY KEY (`P_Zeitpunkt`),
	INDEX `f_BenutzerName` (`f_BenutzerName`),
	INDEX `f_Song_ID` (`f_Song_ID`),
	CONSTRAINT `FK_t_votes_t_benutzer` FOREIGN KEY (`f_BenutzerName`) REFERENCES `t_benutzer` (`p_BenutzerName`) ON UPDATE CASCADE,
	CONSTRAINT `FK_t_votes_t_musik` FOREIGN KEY (`f_Song_ID`) REFERENCES `t_musik` (`p_Song_ID`) ON UPDATE CASCADE
)
;