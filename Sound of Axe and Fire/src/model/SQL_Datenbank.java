package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SQL_Datenbank {
	
	static Connection con = null;
	static String datenbankName = "soundofaxeandfire";
	
	public static void erstelleDatenbank() {
		//Damit die Datenbank automatisch beim Start erstellt wird, sollte hier ein Quellcode erscheinen, der die Tabellen erstellt
	}
	
	public static ArrayList<Musik> getMusikAusDatenbank() {
		ArrayList<Musik> rueckgabe = new ArrayList<Musik>();
		try {
			ResultSet rs = datenbankAbfrage("Select * From t_musik;");
			while (rs.next() == true) {
				rueckgabe.add(new Musik(rs.getString("bandname"), rs.getString("genre"), rs.getString("titelName")));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rueckgabe;
	}
	
	public static boolean addMusikZuDatenbank(Musik eingabe) {
		return datensatzHinzufuegen("INSERT INTO t_musik(bandName,genre,titelName) VALUES ('" + eingabe.getBandname() + "','" + eingabe.getGenre() + "','" + eingabe.getTitelname() + "');");
	}
	
	public static ArrayList<Benutzer> getBenutzerAusDatenbank() {
		ArrayList<Benutzer> rueckgabe = new ArrayList<Benutzer>();
		try {
			ResultSet rs = datenbankAbfrage("SELECT * FROM t_benutzer;");
			while (rs.next()) {
				rueckgabe.add(new Benutzer(rs.getString("p_BenutzerName"), rs.getString("adelsgeschlecht")));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rueckgabe;
	}
	
	public static boolean addBenutzerZuDatenbank(Benutzer eingabe) {
		return datensatzHinzufuegen("INSERT INTO t_benutzer(p_BenutzerName, passwort, adelsgeschlecht, gastgeber) VALUES ('" + eingabe.getBenutzerID() + "', '" + eingabe.getPasswort() + "', '" + eingabe.getAdelsgeschlecht() + "', '" + eingabe.isGastgeber() + "');");
	}
	
	public static ResultSet datenbankAbfrage(String sqlBefehl) {
		neueVerbindung();
		ResultSet rs = null;
		try {
			java.sql.Statement stmt = con.createStatement();
			rs = stmt.executeQuery(sqlBefehl);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return rs;
	}
	
	public static boolean datensatzHinzufuegen(String sqlBefehl) {
		neueVerbindung();
		try {
			java.sql.Statement stmt = con.createStatement();
			stmt.executeUpdate(sqlBefehl);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static void neueVerbindung() {
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/" + datenbankName;
		String user = "root";
		String password = "";
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			con = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}