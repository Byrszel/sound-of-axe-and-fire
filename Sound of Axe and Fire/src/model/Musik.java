package model;

import view.StartGUI;

public class Musik {

	private String bandname;
	private String genre;
	private String titelname;
	
	public Musik(String bandname, String genre, String titelname) {
		super();
		this.bandname = bandname;
		this.genre = genre;
		this.titelname = titelname;
	}

	public String getBandname() {
		return bandname;
	}

	public void setBandname(String bandname) {
		this.bandname = bandname;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getTitelname() {
		return titelname;
	}

	public void setTitelname(String titelname) {
		this.titelname = titelname;
	}

	@Override
	public String toString() {
		return bandname + " - " + titelname + " (" + genre + ")";
	}
	
}