package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import controller.QuickMath;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.JScrollPane;

public class GastGeberGUI extends JFrame {

	private JPanel contentPane;
	private JTable tbl_Playlist;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GastGeberGUI frame = new GastGeberGUI(new QuickMath());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public GastGeberGUI(QuickMath controller) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLabel lbl_Heading = new JLabel("PLAYLIST");
		lbl_Heading.setFont(new Font("Tahoma", Font.PLAIN, 40));
		lbl_Heading.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbl_Heading, BorderLayout.NORTH);
		
		String[] columnNames = {"Bandname", "Titel", "Genre", "Votes"};
		String[][] rowData = new String[controller.getMusikListe().size()][4];
		int[] votes = controller.getVotes();
		for (int i = 0; i < controller.getMusikListe().size(); i++) {
			rowData[i][0] = controller.getMusikListe().get(i).getBandname();
			rowData[i][1] = controller.getMusikListe().get(i).getTitelname();
			rowData[i][2] = controller.getMusikListe().get(i).getGenre();
			rowData[i][3] = Integer.toString(votes[i]);
		}
		tbl_Playlist = new JTable(rowData, columnNames);
		contentPane.add(tbl_Playlist, BorderLayout.CENTER);
		scrollPane = new JScrollPane(tbl_Playlist);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tbl_Playlist.getModel());
	    tbl_Playlist.setRowSorter(sorter);
	}

}
