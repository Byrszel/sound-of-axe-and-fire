package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.GridLayout;

public class ResetGUI extends JFrame {

	private JPanel contentPane;
	private JTextField tfd_Passwort;
	private JTextField tfd_Wiederholen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ResetGUI frame = new ResetGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ResetGUI() {
		setTitle("Passwort \u00E4ndern");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 310, 150);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		tfd_Passwort = new JTextField();
		tfd_Passwort.setText("Neues Passwort");
		panel.add(tfd_Passwort);
		tfd_Passwort.setColumns(10);
		
		tfd_Wiederholen = new JTextField();
		tfd_Wiederholen.setText("Passwort wiederholen");
		panel.add(tfd_Wiederholen);
		tfd_Wiederholen.setColumns(10);
		
		JButton btn_Passwort = new JButton("Best\u00E4tigen");
		panel.add(btn_Passwort);
	}

}
