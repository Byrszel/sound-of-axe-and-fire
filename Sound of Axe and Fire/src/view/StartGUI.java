package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.QuickMath;

import javax.swing.JButton;
import javax.swing.JMenuBar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JTable;

public class StartGUI extends JFrame {

	private JPanel contentPane;
	public static QuickMath controller = new QuickMath();
	static JLabel lbl_eingeloggt_Status;
	static JButton btn_menuBar_Login;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartGUI frame = new StartGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StartGUI() {
		setTitle("Sound of Axe and Fire");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 700);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);	
		
		btn_menuBar_Login = new JButton("");
		btn_menuBar_Login.setIcon(new ImageIcon(StartGUI.class.getResource("/com/sun/javafx/scene/web/skin/FontBackgroundColor_16x16_JFX.png")));
		btn_menuBar_Login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (controller.istJemandAngemeldet()) {
					controller.logout();
				} else {
					LoginGUI loginGUI = new LoginGUI(controller);
					loginGUI.setVisible(true);
				}
			}
		});
		menuBar.add(btn_menuBar_Login);
		
		JButton btn_menuBar_Wunsch = new JButton("Wunsch");
		btn_menuBar_Wunsch.setIcon(new ImageIcon(StartGUI.class.getResource("/com/sun/javafx/scene/web/skin/Underline_16x16_JFX.png")));
		btn_menuBar_Wunsch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (controller.istJemandAngemeldet()) {
					WunschGUI wunschGUI = new WunschGUI(controller);
					wunschGUI.setVisible(true);
				}else {
					PopUpGUI popUp = new PopUpGUI("Bitte melden Sie sich an!");
					popUp.setVisible(true);
				}
			}
		});
		menuBar.add(btn_menuBar_Wunsch);
		
		JButton btn_menuBar_Vote = new JButton("Vote");
		btn_menuBar_Vote.setIcon(new ImageIcon(StartGUI.class.getResource("/com/sun/javafx/scene/web/skin/Paste_16x16_JFX.png")));
		btn_menuBar_Vote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (controller.istJemandAngemeldet()) {
					VoteGUI voteGUI = new VoteGUI(controller);
					voteGUI.setVisible(true);
				}else {
					PopUpGUI popUp = new PopUpGUI("Bitte melden Sie sich an!");
					popUp.setVisible(true);
				}
			}
		});
		menuBar.add(btn_menuBar_Vote);
		
		JButton btn_menuBar_Hilfe = new JButton("Hilfe");
		btn_menuBar_Hilfe.setForeground(Color.RED);
		btn_menuBar_Hilfe.setIcon(new ImageIcon(StartGUI.class.getResource("/com/sun/javafx/scene/web/skin/Copy_16x16_JFX.png")));
		btn_menuBar_Hilfe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HilfeGUI hilfeGUI = new HilfeGUI();
				hilfeGUI.setVisible(true);
				
			}
		});
		menuBar.add(btn_menuBar_Hilfe);
		
		lbl_eingeloggt_Status = new JLabel("");
		lbl_eingeloggt_Status.setHorizontalAlignment(SwingConstants.RIGHT);
		menuBar.add(lbl_eingeloggt_Status);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JButton btn_PartyBeginnen = new JButton("Party beginnen!");
		btn_PartyBeginnen.setIcon(new ImageIcon(StartGUI.class.getResource("/com/sun/javafx/webkit/prism/resources/mediaPlayDisabled.png")));
		btn_PartyBeginnen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (controller.isGastgeber() || controller.isAdelsgeschlecht()) {
					GastGeberGUI gastgeberGUI = new GastGeberGUI(controller);
					gastgeberGUI.setVisible(true);
				}else {
					PopUpGUI popUp = new PopUpGUI("Sie sind nicht der Gastgeber oder haben nicht das passende Adelsgeschlecht!");
					popUp.setVisible(true);
				}
			}
		});
		contentPane.add(btn_PartyBeginnen, BorderLayout.SOUTH);
		
		JLabel lbl_Bild = new JLabel("");
		lbl_Bild.setIcon(new ImageIcon(StartGUI.class.getResource("/view/aktionslogo.png")));
		lbl_Bild.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbl_Bild, BorderLayout.CENTER);
		
		setGUItoLogout();
	}
	
	public static void setGUItoLogin() {
		StartGUI.lbl_eingeloggt_Status.setText("Angemeldet als: " + controller.getAngemeldeterNutzer());
		StartGUI.btn_menuBar_Login.setText("Abmelden");
	}
	
	public static void setGUItoLogout() {
		StartGUI.lbl_eingeloggt_Status.setText("Bitte anmelden!");
		StartGUI.btn_menuBar_Login.setText("Login");
	}
	

}
