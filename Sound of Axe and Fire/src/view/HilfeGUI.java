package view;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.GridLayout;

public class HilfeGUI extends JFrame {

	private JPanel contentPane;
	private JLabel lbl_Hilfe;
	private final JLabel lbl_Credits = new JLabel("Datenbank: Lukas K. | GUI-Designer: Dennis B. | Programmierer: Friedrich P. ");
	private JLabel lbl_Copyright;
	private JLabel lbl_Frage1;
	private JLabel lbl_Antwort1;
	private JLabel lbl_Frage2;
	private JLabel lbl_Antwort2;

	/**
	 * Launch the application.
	 */
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HilfeGUI frame = new HilfeGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public HilfeGUI() {
		setTitle("Sie ben\u00F6tigen Hilfe?");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 614, 295);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		lbl_Copyright = new JLabel("Made by Hentai and Chill \u00A92019, alle Rechte vorbehalten");
		lbl_Copyright.setVerticalAlignment(SwingConstants.TOP);
		lbl_Copyright.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbl_Copyright);
		
		lbl_Hilfe = new JLabel("FAQ");
		lbl_Hilfe.setVerticalAlignment(SwingConstants.TOP);
		lbl_Hilfe.setForeground(Color.RED);
		lbl_Hilfe.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbl_Hilfe);
		
		lbl_Frage1 = new JLabel("Wie nutze Ich das Progamm?");
		lbl_Frage1.setVerticalAlignment(SwingConstants.TOP);
		lbl_Frage1.setForeground(Color.BLUE);
		lbl_Frage1.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbl_Frage1);
		
		lbl_Antwort1 = new JLabel("<html><head></head><body>Um das Programm vollst\u00E4ndig nutzen zu k\u00F6nnen, m\u00FCssen Sie sich zu allererst registrieren. <br>Daf\u00FCr m\u00FCssen Sie auf den Login-Button klicken und den Anweisungen folgen.");
		lbl_Antwort1.setVerticalAlignment(SwingConstants.TOP);
		contentPane.add(lbl_Antwort1);
		
		lbl_Frage2 = new JLabel("Wie oft darf ich f\u00FCr ein Lied voten?");
		lbl_Frage2.setForeground(Color.BLUE);
		lbl_Frage2.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbl_Frage2);
		
		lbl_Antwort2 = new JLabel("<html><head></head><body>Sie d\u00FCrfen unendlich viele Votes t\u00E4tigen, jedoch m\u00FCssen Sie ber\u00FCcksichtigen, dass wir aufgrund unseres Gastgebers<break> lediglich die letzten f\u00FCnf Votes z\u00E4hlen d\u00FCrfen. Tut uns leid :(");
		contentPane.add(lbl_Antwort2);
		lbl_Credits.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Credits.setVerticalAlignment(SwingConstants.BOTTOM);
		contentPane.add(lbl_Credits);
	}

}
