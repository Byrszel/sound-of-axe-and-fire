package view;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.QuickMath;
import model.Benutzer;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class VoteGUI extends JFrame {

	private JPanel contentPane;
	static JLabel[] lbl_Vote = new JLabel[Benutzer.maxVotesProPerson];

	/**
	 * Launch the application.
	 */
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VoteGUI frame = new VoteGUI(new QuickMath());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 * @param controller 
	 */
	public VoteGUI(QuickMath controller) {
		setTitle("VoteGUI");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 900, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel pnl_VoteListe = new JPanel();
		contentPane.add(pnl_VoteListe, BorderLayout.CENTER);
		pnl_VoteListe.setLayout(new GridLayout(controller.getMusikListe().size(), 2, 0, 0));
		
		JScrollPane scrollPane = new JScrollPane(pnl_VoteListe);
		contentPane.add(scrollPane);
		
		JButton[] btn_Vote = new JButton[controller.getMusikListe().size()];
		for (int i = 0; i < controller.getMusikListe().size(); i++) {
			JLabel lbl_Lied = new JLabel(controller.getMusikListe().get(i).getTitelname() + " - " + controller.getMusikListe().get(i).getBandname());
			lbl_Lied.setHorizontalAlignment(SwingConstants.CENTER);
			pnl_VoteListe.add(lbl_Lied);
			
			btn_Vote[i] = new JButton(i + "");
			btn_Vote[i].setHorizontalAlignment(SwingConstants.CENTER);
			btn_Vote[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String zwischenspeicher = ((JButton) e.getSource()).getText();
					controller.voten(Integer.parseInt(zwischenspeicher));
				}
			});
			pnl_VoteListe.add(btn_Vote[i]);
		}
		
		JButton btn_Bestštigen = new JButton("Best\u00E4tigen");
		btn_Bestštigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		contentPane.add(btn_Bestštigen, BorderLayout.SOUTH);
		
		JPanel pnl_currentVotes = new JPanel();
		contentPane.add(pnl_currentVotes, BorderLayout.EAST);
		pnl_currentVotes.setLayout(new GridLayout(Benutzer.maxVotesProPerson, 0, 0, 0));
		for (int i = 0; i < Benutzer.maxVotesProPerson; i++) {
			lbl_Vote[i] = new JLabel((i+1) + ": ");
			votenGuiUpdate();
			lbl_Vote[i].setHorizontalAlignment(SwingConstants.LEFT);
			pnl_currentVotes.add(lbl_Vote[i]);
		}
	}
	
	public static void votenGuiUpdate() {
		for (int i = 0; i < Benutzer.maxVotesProPerson; i++) {
			if (StartGUI.controller.getVotesOfCurrentUser()[i].getSongID() != -1) {
				lbl_Vote[i].setText((i+1) + ": " + StartGUI.controller.getMusikListe().get(StartGUI.controller.getVotesOfCurrentUser()[i].getSongID()).toString());
			}
		}
	}

}
