package view;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.QuickMath;
import model.Benutzer;
import model.JTextFieldMitKrassemStandardtext;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;

public class LoginGUI extends JFrame {

	private JPanel contentPane;
	private JTextFieldMitKrassemStandardtext tfd_Benutzername = new JTextFieldMitKrassemStandardtext("Benutzername");
	private JButton btn_Login;
	private JButton btn_Registrieren;
	private JPanel pnl_North;
	private JTextFieldMitKrassemStandardtext tfd_Adelsgeschlecht = new JTextFieldMitKrassemStandardtext("Adelsgeschlecht");
	private JButton btn_PWVergessen;
	private JTextFieldMitKrassemStandardtext tfd_Passwort = new JTextFieldMitKrassemStandardtext("Passwort");
	private JCheckBox chckbx_Gastgeber;

	/**
	 * Launch the application.
	 */
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginGUI frame = new LoginGUI(new QuickMath());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 * @param controller 
	 */
	public LoginGUI(QuickMath controller) {
		setTitle("LoginGUI");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 320, 190);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		btn_Login = new JButton("Einloggen");
		btn_Login.setIcon(new ImageIcon(LoginGUI.class.getResource("/com/sun/javafx/webkit/prism/resources/mediaPlayDisabled.png")));
		btn_Login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (controller.login(tfd_Benutzername.getText())) {
					PopUpGUI popUpGUI = new PopUpGUI("Login Erfolgreich.");
					popUpGUI.setVisible(true);
					dispose();
				}else {
					PopUpGUI popUpGUI = new PopUpGUI("Benutzer existiert nicht!");
					popUpGUI.setVisible(true);
				}
			}
		});
		
		btn_Registrieren = new JButton("Registrieren");
		btn_Registrieren.setIcon(new ImageIcon(LoginGUI.class.getResource("/com/sun/javafx/scene/control/skin/caspian/images/capslock-icon.png")));
		btn_Registrieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (controller.addBenutzerListe(new Benutzer(tfd_Benutzername.getText(), tfd_Adelsgeschlecht.getText()))) {
					PopUpGUI popUpGUI = new PopUpGUI("Registrierung erfolgreich, Sie k\u00F6nnen sich nun mit Ihrem Benutzernamen einloggen.");
					popUpGUI.setVisible(true);
				}else {
					PopUpGUI popUpGUI = new PopUpGUI("Registrierung nicht erfolgreich, Benutzername existiert bereits!");
					popUpGUI.setVisible(true);
				}
				
			}
		});
		
		panel.add(btn_Registrieren);
		panel.add(btn_Login);
		
		pnl_North = new JPanel();
		contentPane.add(pnl_North, BorderLayout.NORTH);
		pnl_North.setLayout(new GridLayout(4, 1, 0, 0));
		
		pnl_North.add(tfd_Benutzername);
		tfd_Benutzername.setColumns(10);
		
		pnl_North.add(tfd_Passwort);
		tfd_Passwort.setColumns(10);
		
		pnl_North.add(tfd_Adelsgeschlecht);
		tfd_Adelsgeschlecht.setColumns(10);
		
		chckbx_Gastgeber = new JCheckBox("Gastgeber");
		pnl_North.add(chckbx_Gastgeber);
		
		btn_PWVergessen = new JButton("Passwort vergessen?");
		btn_PWVergessen.setIcon(new ImageIcon(LoginGUI.class.getResource("/com/sun/javafx/scene/web/skin/FontBackgroundColor_16x16_JFX.png")));
		btn_PWVergessen.setSelectedIcon(new ImageIcon(LoginGUI.class.getResource("/com/sun/javafx/scene/web/skin/FontBackgroundColor_16x16_JFX.png")));
		contentPane.add(btn_PWVergessen, BorderLayout.SOUTH);
	}

}
