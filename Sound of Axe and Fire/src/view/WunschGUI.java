package view;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.QuickMath;
import model.JTextFieldMitKrassemStandardtext;
import model.Musik;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class WunschGUI extends JFrame {

	private JPanel contentPane;
	private JTextFieldMitKrassemStandardtext tfd_Liedname = new JTextFieldMitKrassemStandardtext("Liedname");
	private JTextFieldMitKrassemStandardtext tfd_Genre = new JTextFieldMitKrassemStandardtext("Genre");
	private JTextFieldMitKrassemStandardtext tfd_Bandname = new JTextFieldMitKrassemStandardtext("Bandname");

	/**
	 * Launch the application.
	 */
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WunschGUI frame = new WunschGUI(new QuickMath());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public WunschGUI(QuickMath controller) {
		setTitle("Wir sind doch nicht hier bei \"W\u00FCnsch dir was!\"");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 480, 270);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		
		JButton btn_Wunsch = new JButton("Bestštigen");
		btn_Wunsch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.addMusikListe(new Musik(tfd_Bandname.getText(), tfd_Genre.getText(), tfd_Liedname.getText()));
				dispose();
			}
		});
		
		panel.add(tfd_Liedname);
		tfd_Liedname.setText("Liedname");
		tfd_Liedname.setColumns(10);
		
		tfd_Bandname.setText("Bandname");
		panel.add(tfd_Bandname);
		tfd_Bandname.setColumns(10);
		
		tfd_Genre.setText("Genre");
		panel.add(tfd_Genre);
		tfd_Genre.setColumns(10);
		panel.add(btn_Wunsch);
	}

}
